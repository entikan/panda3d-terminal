import os
import sys
import pty
import select
from time import sleep
from subprocess import Popen, PIPE

import pyte
from panda3d.core import TextNode
from direct.stdpy import thread


class TerminalNode:
    def __init__(self):
        self.active = True
        self.text_node = TextNode("terminal")
        self.text_node_path = aspect2d.attach_new_node(self.text_node)
        self.text_node_path.set_scale(0.04)
        self.text_node_path.set_pos(-0.9,0,0.9)

        self.master, self.slave = pty.openpty()
        p = Popen(["/bin/bash", "-is"],
            stdin = self.slave, stdout = self.master, stderr = PIPE,
        )

        self.tsize = [70, 40]
        self.screen = pyte.Screen(*self.tsize)
        self.stream = pyte.ByteStream(self.screen)
        thread.start_new_thread(self.read_data, args=[])
        base.task_mgr.add(self.update)

    def read_data(self):
        while True:
            self.stream.feed(os.read(self.master, 1024))

    def update(self, task):
        self.text_node.text = "\n".join(self.screen.display)
        return task.cont


if __name__ == "__main__":
    from direct.showbase.ShowBase import ShowBase
    from panda3d.core import loadPrcFileData


    loadPrcFileData('', 'text-default-font unifont.otf')

    base = ShowBase()
    base.win.set_clear_color((0.05,0.05,0.05,1))
    base.cam.set_pos(0,-100,5)
    base.accept("escape", sys.exit)
    base.terminal = TerminalNode()
    base.run()
